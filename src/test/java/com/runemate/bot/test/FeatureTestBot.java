package com.runemate.bot.test;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.listeners.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import com.runemate.game.incubating.input.*;
import com.runemate.rmi.*;
import com.runemate.ui.setting.annotation.open.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.concurrent.*;
import javax.swing.Timer;
import javax.swing.*;
import lombok.*;
import lombok.extern.log4j.*;

/**
 * This bot can be used to test new features that you're working on.Recommend that you use this to make it obvious to the reviewer
 * how exactly the code change was tested.
 * <p>
 * The "testJar" gradle task will produce a jar containing this bot which you can place in a bot directory.
 * <p>
 * The following gradle command will build this bot and then launch the client:
 * testJar launch --args="--dev"
 */
@Log4j2
public class FeatureTestBot extends LoopingBot implements NpcListener {

    @SettingsProvider(updatable = true)
    private ExampleSettings settings;

    JFrame frame;
    Overlay overlay;

    private final List<Npc> spawns = new ArrayList<>();

    @Override
    public void onStart(final String... arguments) {
        getEventDispatcher().addListener(this);
        setLoopDelay(1200);
    }

    @Override
    public void onLoop() {

    }

    private void startOverlay() {
        frame = new JFrame("Model viewer");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocation(Screen.getLocation());
        frame.setBounds(Screen.getBounds());
        frame.setAlwaysOnTop(true);

        overlay = new Overlay();
        frame.add(overlay);
        frame.setUndecorated(true);
        frame.setBackground(new Color(0, 0, 0, 0));

        frame.pack();
        frame.setVisible(true);
    }

    @Override
    public void onStop(final String reason) {

    }

    @Override
    public void onNpcSpawned(final NpcSpawnedEvent event) {
        spawns.add(event.getNpc());
        log.info(event);
    }

    private boolean equals(Npc one, Npc two) {
        return BridgeUtil.getFieldInstance(((Entity) one).uid) == BridgeUtil.getFieldInstance(((Entity) two).uid);
    }

    public class Overlay extends JComponent {

        private final javax.swing.Timer timer;

        public Overlay() {
            timer = new Timer(100, e -> {
                // Code to update the overlay every 50 ms goes here
                try {
                    getPlatform().invokeAndWait(() -> {
                        frame.setLocation(Screen.getLocation());
                        frame.setSize(Screen.getBounds().getSize());
                    });
                } catch (ExecutionException | InterruptedException ignored) {

                }
                repaint();
            });
            timer.start(); // Start the timer
        }

        @SneakyThrows
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            // Code to render the overlay goes here
            var graphics = (Graphics2D) g;
            graphics.setPaint(Color.RED);
            graphics.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
            getPlatform().invokeAndWait(() -> {

                final var player = Players.getLocal();
                if (player != null) {
                    player.getServerPosition().render(graphics);
                }

                final var object = GameObjects.newQuery().types(GameObject.Type.PRIMARY).results().nearest();
                if (object != null) {
                    graphics.setPaint(Color.BLUE);
                    var clickbox = OpenHull.lookup(((Entity) object).uid);
                    if (clickbox != null) {
                        graphics.draw(clickbox);
                    }

                    var model = object.getModel();
                    if (model != null) {
                        log.info("Model type {}", model.getClass().getSimpleName());
                        graphics.setPaint(Color.RED);
                        model.render(graphics);
                    }
                }

                final var mouse = Mouse.getPosition();
                graphics.drawRect((int) mouse.getX(), (int) mouse.getY(), 2, 2);
            });
        }


    }
}
