package com.runemate.game.api.hybrid.region;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.osrs.region.*;
import java.util.*;
import java.util.function.*;

/**
 * For retrieval, sorting, and analysis of GameObjects
 */
public final class GameObjects {
    private GameObjects() {
    }

    /**
     * Creates a new game object query.
     */
    public static GameObjectQueryBuilder newQuery() {
        return new GameObjectQueryBuilder();
    }

    /**
     * Gets a Set of all loaded GameObjects
     */
    public static LocatableEntityQueryResults<GameObject> getLoaded() {
        return getLoaded((Predicate<GameObject>) null);
    }

    /**
     * Gets a Set of all loaded GameObjects with one of the given ids
     */
    public static LocatableEntityQueryResults<GameObject> getLoaded(final int... ids) {
        return getLoaded(getIdPredicate(ids));
    }

    /**
     * Gets a Set of all loaded GameObjects with one of the given names
     */
    public static LocatableEntityQueryResults<GameObject> getLoaded(final String... names) {
        return getLoaded(getNamePredicate(names));
    }

    /**
     * Gets a Set of all loaded GameObjects that are accepted by the given filter
     */
    public static LocatableEntityQueryResults<GameObject> getLoaded(
        final Predicate<GameObject> filter
    ) {
        return OSRSGameObjects.getLoaded(filter);
    }

    /**
     * Gets a Set of all loaded GameObjects at the given Coordinate
     */
    public static LocatableEntityQueryResults<GameObject> getLoadedOn(final Coordinate coordinate) {
        return getLoadedOn(coordinate, (Predicate<GameObject>) null);
    }

    /**
     * Gets a Set of all loaded GameObjects at the given coordinate with one of the given ids
     */
    public static LocatableEntityQueryResults<GameObject> getLoadedOn(
        final Coordinate coordinate,
        final int... ids
    ) {
        return getLoadedOn(coordinate, getIdPredicate(ids));
    }

    /**
     * Gets a Set of all loaded GameObjects at the given coordinate with one of the given names
     */
    public static LocatableEntityQueryResults<GameObject> getLoadedOn(
        final Coordinate coordinate,
        final String... names
    ) {
        return getLoadedOn(coordinate, getNamePredicate(names));
    }

    /**
     * Gets a Set of all loaded GameObjects at the given coordinate that is accepted by the filter
     */
    public static LocatableEntityQueryResults<GameObject> getLoadedOn(
        final Coordinate coordinate,
        final Predicate<GameObject> filter
    ) {
        return OSRSGameObjects.getLoadedOn(coordinate, filter);
    }

    /**
     * Gets a Set of all loaded GameObjects within the given Area
     */
    public static LocatableEntityQueryResults<GameObject> getLoadedWithin(final Area area) {
        return getLoadedWithin(area, (Predicate<GameObject>) null);
    }

    /**
     * Gets a Set of all loaded GameObjects within the given Area that are accepted by the filter
     */
    public static LocatableEntityQueryResults<GameObject> getLoadedWithin(
        final Area area,
        final Predicate<GameObject> filter
    ) {
        return OSRSGameObjects.getLoadedWithin(area, filter);
    }

    public static LocatableEntityQueryResults<GameObject> getLoadedWithin(
        final Area area,
        final int... ids
    ) {
        return getLoadedWithin(area, getIdPredicate(ids));
    }

    public static LocatableEntityQueryResults<GameObject> getLoadedWithin(
        final Area area,
        final String... names
    ) {
        return getLoadedWithin(area, getNamePredicate(names));
    }

    public static Predicate<GameObject> getIdPredicate(final int... acceptedIds) {
        return gameObject -> {
            final int id = gameObject.getId();
            for (int accepted : acceptedIds) {
                if (accepted == id) {
                    return true;
                }
            }
            return false;
        };
    }

    public static Predicate<GameObject> getAreaPredicate(final Area... acceptedAreas) {
        return object -> {
            for (final Area area : acceptedAreas) {
                if (area.contains(object)) {
                    return true;
                }
            }
            return false;
        };
    }

    public static Predicate<GameObject> getTypePredicate(final GameObject.Type... acceptedTypes) {
        return object -> {
            for (final GameObject.Type type : acceptedTypes) {
                if (type == object.getType()) {
                    return true;
                }
            }
            return false;
        };
    }

    /**
     * Gets a GameObject filter that can be used to get a GameObject with one of the specified action
     *
     * @param acceptedActions the powers that are valid (case-sensitive)
     * @return a filter
     */
    public static Predicate<GameObject> getActionPredicate(final String... acceptedActions) {
        return object -> {
            GameObjectDefinition def = object.getDefinition();
            if (def != null) {
                GameObjectDefinition local = def.getLocalState();
                if (local != null) {
                    def = local;
                }
            }
            if (def != null) {
                for (final String action : def.getActions()) {
                    for (final String acceptedAction : acceptedActions) {
                        if (action.equals(acceptedAction)) {
                            return true;
                        }
                    }
                }
            }
            return false;
        };
    }

    /**
     * Gets a GameObject filter that can be used to get a GameObject with one of the specified names
     *
     * @param acceptedNames the names that are valid (case-sensitive)
     * @return a filter
     */
    public static Predicate<GameObject> getNamePredicate(final String... acceptedNames) {
        return object -> {
            GameObjectDefinition def = object.getDefinition();
            if (def != null) {
                String name = def.getName();
                if (name != null) {
                    for (final String acceptedName : acceptedNames) {
                        if (name.equals(acceptedName)) {
                            return true;
                        }
                    }
                }
                def = def.getLocalState();
                if (def != null) {
                    name = def.getName();
                    if (name != null) {
                        for (final String acceptedName : acceptedNames) {
                            if (name.equals(acceptedName)) {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        };
    }

    public static Predicate<GameObject> getAppearancePredicate(final int... acceptedAppearanceIds) {
        return object -> {
            GameObjectDefinition def = object.getDefinition();
            if (def != null) {
                for (int appearanceId : def.getAppearance()) {
                    for (int accepted : acceptedAppearanceIds) {
                        if (appearanceId == accepted) {
                            return true;
                        }
                    }
                }
                def = def.getLocalState();
                if (def != null) {
                    for (int appearanceId : def.getAppearance()) {
                        for (int accepted : acceptedAppearanceIds) {
                            if (appearanceId == accepted) {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        };
    }

    /**
     * Gets a GameObject model hash filter
     *
     * @param acceptedHashes
     */
    public static Predicate<GameObject> getModelPredicate(final int... acceptedHashes) {
        return new Predicate<GameObject>() {
            private final HashMap<Integer, Integer> idToHash = new HashMap<>();

            @Override
            public boolean test(GameObject gameObject) {
                int objectId = gameObject.getId();
                if (!idToHash.containsKey(objectId)) {
                    final Model model = gameObject.getModel();
                    if (model != null) {
                        idToHash.put(objectId, model.hashCode());
                    }
                }
                if (idToHash.containsKey(objectId)) {
                    final int hash = idToHash.get(objectId);
                    for (int accepted : acceptedHashes) {
                        if (hash == accepted) {
                            return true;
                        }
                    }
                }
                return false;
            }
        };
    }

    public static Predicate<GameObject> getSizePredicate(final int width, final int height) {
        return gameObject -> {
            final Area.Rectangular area = gameObject.getArea();
            return area != null && area.getWidth() == width && area.getHeight() == height;
        };
    }
}
