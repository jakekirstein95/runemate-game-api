/*
 * Copyright (c) 2017, Adam <Adam@sigterm.info>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.runemate.game.api.hybrid.structures;

import com.runemate.game.api.hybrid.util.shapes.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

/**
 * Implemented nearly identically to how RuneLite implements it except we can handle the projected triangle faces.
 */
public class ConvexHulls {
    public static Polygon jarvisMarchOnFaces(List<Triangle> faces) {
        Collection<Point> vertices = faces.stream()
            .map(triangle -> Arrays.asList(triangle.getA(), triangle.getB(), triangle.getC()))
            .flatMap((Function<List<Point>, Stream<Point>>) Collection::stream)
            .collect(Collectors.toSet());
        return jarvisMarchOnVertices(vertices);
    }

    public static Polygon jarvisMarchOnVertices(Collection<Point> points) {
        if (points.size() < 3) {
            return null;
        }

        List<Point> ch = new ArrayList<>();
        // find the left most point
        Point left = getFurthestLeft(points);
        // current point we are on
        Point current = left;
        do {
            ch.add(current);
            assert ch.size() <= points.size() : "hull has more points than graph";
            if (ch.size() > points.size()) {
                // Just to make sure we never somehow get stuck in this loop
                return null;
            }
            // the next point - all points are to the right of the
            // line between current and next
            Point next = null;
            for (Point p : points) {
                if (next == null) {
                    next = p;
                    continue;
                }
                long cp = crossProduct(current, p, next);
                if (cp > 0 || (cp == 0 && distanceTo(current, p) > distanceTo(current, next))) {
                    next = p;
                }
            }
            // Points can be null if they are behind or very close to the camera.
            if (next == null) {
                return null;
            }
            current = next;
        }
        while (current != left);
        if (ch == null) {
            return null;
        }
        // Convert to a polygon
        Polygon p = new Polygon();
        for (Point point : ch) {
            p.addPoint(point.x, point.y);
        }
        return p;
    }

    private static Point getFurthestLeft(Collection<Point> vertices) {
        Point left = null;
        for (Point p : vertices) {
            if (left == null || p.x < left.x) {
                left = p;
            } else if (p.x == left.x && p.y < left.y) {
                left = p;
            }
        }
        return left;
    }

    private static long crossProduct(Point p, Point q, Point r) {
        return (long) (q.y - p.y) * (r.x - q.x) - (long) (q.x - p.x) * (r.y - q.y);
    }

    private static int distanceTo(Point current, Point other) {
        return (int) Math.hypot(current.x - other.x, current.y - other.y);
    }
}
