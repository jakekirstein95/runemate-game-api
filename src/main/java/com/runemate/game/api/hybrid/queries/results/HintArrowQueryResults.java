package com.runemate.game.api.hybrid.queries.results;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.calculations.*;
import java.util.*;
import java.util.concurrent.*;
import javax.annotation.*;

public class HintArrowQueryResults extends QueryResults<HintArrow, HintArrowQueryResults> {
    public HintArrowQueryResults(final Collection<? extends HintArrow> results) {
        super(results);
    }

    public HintArrowQueryResults(
        final Collection<? extends HintArrow> results,
        ConcurrentMap<String, Object> cache
    ) {
        super(results, cache);
    }

    @Override
    protected HintArrowQueryResults get() {
        return this;
    }

    /**
     * Calls sort(Comparator) with a Comparator that will sort the list by distance from the local player (nearest first)
     */
    public final HintArrowQueryResults sortByDistance() {
        return sortByDistanceFrom(Players.getLocal());
    }

    /**
     * Calls sort(Comparator) with a Comparator that will sort the list by distance from the local player (nearest first)
     */
    public final HintArrowQueryResults sortByDistanceFrom(Locatable center) {
        return sort(new CachingDistanceComparator(center, Distance.Algorithm.MANHATTAN));
    }

    public final HintArrowQueryResults sortByDistanceFrom(
        Locatable center,
        Distance.Algorithm algorithm
    ) {
        return sort(new CachingDistanceComparator(center, algorithm));
    }


    /**
     * Gets the nearest entity to the local player.
     * If two or more entities are tied for the place of nearest then one will be selected according to the active player sense profile.
     */
    @Nullable
    public final HintArrow nearest() {
        return nearestTo(Players.getLocal());
    }

    /**
     * Gets the nearest entity to the given locatable.
     * If two or more entities are tied for the place of nearest then one will be selected according to the active player sense profile.
     */
    @Nullable
    public final HintArrow nearestTo(final Locatable locatable) {
        if (locatable == null || size() == 0) {
            return null;
        }
        List<HintArrow> sorted = Sort.byDistanceFrom(locatable, this.backingList);
        List<HintArrow> equidistant = new ArrayList<>(3);
        double minimum = -1;
        for (HintArrow entity : sorted) {
            double distance = Distance.between(locatable, entity);
            if (minimum == -1) {
                minimum = distance;
            } else if (minimum != distance) {
                break;
            }
            equidistant.add(entity);
        }
        HintArrow preferred = null;
        minimum = -1;
        int tiebreaker = PlayerSense.getAsInteger(PlayerSense.Key.DISTANCE_ANGLE_TIE_BREAKER);
        for (HintArrow entity : equidistant) {
            int distance = CommonMath.getDistanceBetweenAngles(
                tiebreaker,
                CommonMath.getAngleOf(locatable, entity)
            );
            if (minimum == -1 || distance < minimum) {
                minimum = distance;
                preferred = entity;
            }
        }
        return preferred;
    }

    /**
     * Gets the furthest entity to the local player.
     * If two or more entities are tied for the place of furthest then one will be selected according to the active player sense profile.
     */
    @Nullable
    public final HintArrow furthest() {
        return furthestFrom(Players.getLocal());
    }

    /**
     * Gets the furthest entity to the given locatable.
     * If two or more entities are tied for the place of furthest then one will be selected according to the active player sense profile.
     */
    @Nullable
    public final HintArrow furthestFrom(final Locatable locatable) {
        if (locatable == null) {
            return null;
        }
        List<HintArrow> sorted = Sort.byDistanceFrom(locatable, this.backingList);
        Collections.reverse(sorted);
        List<HintArrow> equidistant = new ArrayList<>(3);
        double maximum = -1;
        for (HintArrow entity : sorted) {
            double distance = Distance.between(locatable, entity);
            if (maximum == -1) {
                maximum = distance;
            } else if (maximum != distance) {
                break;
            }
            equidistant.add(entity);
        }
        HintArrow preferred = null;
        double minimum = -1;
        int tiebreaker = PlayerSense.getAsInteger(PlayerSense.Key.DISTANCE_ANGLE_TIE_BREAKER);
        for (HintArrow entity : equidistant) {
            int distance = CommonMath.getDistanceBetweenAngles(
                tiebreaker,
                CommonMath.getAngleOf(locatable, entity)
            );
            if (minimum == -1 || distance < minimum) {
                minimum = distance;
                preferred = entity;
            }
        }
        return preferred;
    }
}
