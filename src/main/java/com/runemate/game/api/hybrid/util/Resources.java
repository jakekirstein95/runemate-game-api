package com.runemate.game.api.hybrid.util;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.location.navigation.web.*;
import com.runemate.game.api.script.data.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.internal.exception.*;
import com.runemate.io.*;
import java.awt.image.*;
import java.io.*;
import java.net.*;
import java.nio.charset.*;
import java.util.*;
import javax.imageio.*;

public final class Resources {
    @Deprecated
    public static URL getAsURL(String name) {
        AbstractBot script = Environment.getBot();
        if (script == null) {
            return null;
        }
        return script.getClass().getClassLoader().getResource(fixName(name));
    }

    public static InputStream getAsStream(AbstractBot bot, String name) {
        if (bot == null) {
            return null;
        }
        final ClassLoader loader = bot.getClass().getClassLoader();
        final ScriptMetaData md;
        if ((md = bot.getMetaData()) != null && md.isLocal() && !resourceDeclared(name, bot)) {
            throw new ResourceNotDeclaredException(
                String.format("Resource not declared in manifest: %s", name));
        }
        return loader.getResourceAsStream(fixName(name));
    }

    public static InputStream getAsStream(String name) {
        return getAsStream(Environment.getBot(), name);
    }

    public static byte[] getAsByteArray(AbstractBot bot, String name) throws IOException {
        InputStream stream = getAsStream(bot, name);
        if (stream == null) {
            return null;
        }
        try (IOTunnel tunnel = new IOTunnel(stream)) {
            return tunnel.readAsArray();
        }
    }

    public static byte[] getAsByteArray(String name) throws IOException {
        return getAsByteArray(Environment.getBot(), name);
    }

    public static String getAsString(AbstractBot bot, String name, Charset encoding)
        throws IOException {
        byte[] result = getAsByteArray(bot, name);
        if (result == null) {
            return null;
        }
        return new String(result, encoding);
    }

    public static String getAsString(String name, Charset encoding) throws IOException {
        return getAsString(Environment.getBot(), name, encoding);
    }

    public static BufferedImage getAsBufferedImage(AbstractBot bot, String name)
        throws IOException {
        InputStream stream = getAsStream(bot, name);
        if (stream == null) {
            return null;
        }
        return ImageIO.read(stream);
    }

    public static BufferedImage getAsBufferedImage(String name) throws IOException {
        return getAsBufferedImage(Environment.getBot(), name);
    }

    public static SerializableWeb getAsSerializedWeb(AbstractBot bot, String name)
        throws IOException, ClassNotFoundException {
        byte[] result = getAsByteArray(bot, name);
        if (result == null) {
            return null;
        }
        return SerializableWeb.deserialize(result);
    }

    public static SerializableWeb getAsSerializedWeb(String name)
        throws IOException, ClassNotFoundException {
        return getAsSerializedWeb(Environment.getBot(), name);
    }

    private static String fixName(String name) {
        /*name = name.replace(".", "/").replace("\"", "/");
        if (name.startsWith("/")) {
            name = name.substring(1);
        }*/
        return name;
    }

    private static boolean resourceDeclared(final String name, final AbstractBot bot) {
        final String fileName = Arrays.stream(name.split("/")).reduce((f, s) -> s).orElse(name);
        for (final String declared : bot.getMetaData().getDeclaredResources()) {
            if (declared.endsWith("/")) {
                if (Objects.equals(declared + fileName, name)) {
                    return true;
                }
            }
            if (declared.equals(name)) {
                return true;
            }
        }
        return false;
    }
}
