package com.runemate.game.api.hybrid.cache.elements;

import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.cache.io.*;
import com.runemate.game.cache.item.*;
import java.io.*;

public class MapFileCacheExtract implements DecodedItem {
    private final boolean rs3;

    private byte[][][] renderFlags;
    private byte[][][] tileShapes;
    private byte[][][] tileOrientations;
    private int[][][] overlayIds, underlayIds;

    public MapFileCacheExtract(Coordinate ignored_base, boolean rs3) {
        this.rs3 = rs3;
    }

    @Override
    public void decode(Js5InputStream stream) throws IOException {
        renderFlags = new byte[4][64][64];
        tileShapes = new byte[4][64][64];
        tileOrientations = new byte[4][64][64];
        overlayIds = new int[4][64][64];
        underlayIds = new int[4][64][64];
        for (int plane = 0; plane < 4; plane++) {
            for (int x = 0; x < 64; x++) {
                for (int y = 0; y < 64; y++) {
                    if (rs3) {
                        int opcode = stream.readUnsignedByte();
                        if ((opcode & 0x1) != 0) {
                            int tile_info = stream.readUnsignedByte();
                            tileShapes[plane][x][y] = (byte) (tile_info >> 2);
                            tileOrientations[plane][x][y] =
                                (byte) ((tile_info/*+region_rotation*/) & 0x3);
                            overlayIds[plane][x][y] = stream.readShortSmart();
                        }
                        if ((opcode & 0x2) != 0) {
                            renderFlags[plane][x][y] = stream.readByte();
                        }
                        if ((opcode & 0x4) != 0) {
                            underlayIds[plane][x][y] = stream.readShortSmart();
                        }
                        if ((opcode & 0x8) != 0) {
                            stream.readUnsignedByte();
                        }
                    } else {
                        while (true) {
                            int opcode = stream.readUnsignedByte();
                            if (opcode == 0) {
                                //Typically height calculations are done
                                break;
                            } else if (opcode == 1) {
                                int height = stream.readUnsignedByte();
                                //Typically height calculations are done
                                break;
                            } else if (opcode <= 49) {
                                overlayIds[plane][x][y] = stream.readByte();
                                tileShapes[plane][x][y] = (byte) ((opcode - 2) >> 2);
                                tileOrientations[plane][x][y] = (byte) (opcode - 2 & 0x3);
                            } else if (opcode <= 81) {
                                renderFlags[plane][x][y] = (byte) (opcode - 49);
                            } else {
                                underlayIds[plane][x][y] = (byte) (opcode - 81);
                            }
                        }
                    }
                }
            }
        }
    }

    public byte[][][] getRenderFlags() {
        return renderFlags;
    }

    public byte[][][] getTileShapes() {
        return tileShapes;
    }

    public byte[][][] getTileOrientations() {
        return tileOrientations;
    }

    public int[][][] getOverlayIds() {
        return overlayIds;
    }

    public int[][][] getUnderlayIds() {
        return underlayIds;
    }

    /**
     * These values are responsible for 13% of memory consumption when building the collision graphs
     */
    public void invalidate() {
        this.renderFlags = null;
        this.tileShapes = null;
        this.tileOrientations = null;
        this.overlayIds = null;
        this.underlayIds = null;
    }
}
