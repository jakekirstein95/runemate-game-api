package com.runemate.game.api.hybrid.cache.elements;

import com.google.common.primitives.*;
import com.runemate.game.api.hybrid.cache.configs.*;
import com.runemate.game.cache.io.*;
import com.runemate.game.cache.item.*;
import java.io.*;
import java.util.*;

public class CacheEnumDefinition extends IncrementallyDecodedItem implements EnumDefinition {
    private final int id;
    private char keyType;
    private char valueType;
    private int defaultInt;
    private String defaultString = "null";
    private HashMap<Serializable, int[]> inverses;
    private HashMap<Integer, Serializable> map;
    private int size;
    private Serializable[] array;

    public CacheEnumDefinition(int id) {
        this.id = id;
    }

    @SuppressWarnings("deprecation")
    private static char replaceLatin1(byte b) {
        int c = b & 0xff;
        if (c == 0) {
            throw new IllegalArgumentException(
                "Non cp1252 character 0x" + Integer.toString(c, 16) + " provided");
        }
        if (c >= 0x80 && c < 0xa0) {
            int unicode = Js5InputStream.getCP1252ReplacementAt(c - 0x80);
            if (unicode == 0) {
                unicode = 0x3f;
            }
            c = unicode;
        }
        return (char) c;
    }

    @Override
    protected void decode(Js5InputStream stream, int opcode) throws IOException {
        if (opcode == 1) {
            keyType = replaceLatin1(stream.readByte());
        } else if (opcode == 2) {
            valueType = replaceLatin1(stream.readByte());
        } else if (opcode == 3) {
            defaultString = stream.readCStyleLatin1String();
        } else if (opcode == 4) {
            defaultInt = stream.readInt();
        } else if (opcode == 5 || opcode == 6) {
            size = stream.readUnsignedShort();
            map = new HashMap<>(size);
            for (int index = 0; index < this.size; index++) {
                int key = stream.readInt();
                Serializable value;
                if (5 == opcode) {
                    value = stream.readCStyleLatin1String();
                } else {
                    value = stream.readInt();
                }
                this.map.put(key, value);
            }
        } else if (opcode == 7 || opcode == 8) {
            int valueCount = stream.readUnsignedShort();
            this.size = stream.readUnsignedShort();
            this.array = new Serializable[valueCount];
            for (int count = 0; count < this.size; ++count) {
                int index = stream.readUnsignedShort();
                if (opcode == 7) {
                    this.array[index] = stream.readCStyleLatin1String();
                } else {
                    this.array[index] = stream.readInt();
                }
            }
        } else if (opcode == 101) {
            keyType = (char) stream.readShortSmart();
        } else if (opcode == 102) {
            valueType = (char) stream.readShortSmart();
        } else {
            throw new DecodingException(opcode);
        }
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public int[] getKeys() {
        if (this.array != null) {
            int[] keys = new int[this.array.length];
            for (int i = 0; i < this.array.length; i++) {
                keys[i] = i;
            }
            return keys;
        } else {
            return this.map != null ? Ints.toArray(this.map.keySet()) : null;
        }
    }

    @Override
    public int getEntryCount() {
        return size;
    }

    @Override
    public Serializable get(int key) {
        if (this.array != null) {
            return key >= 0 && key < this.array.length ? this.array[key] : null;
        } else {
            return this.map != null ? this.map.get(key) : null;
        }
    }

    @Override
    public Serializable get(int key, Serializable dflt) {
        Serializable value = get(key);
        return value != null ? value : dflt;
    }

    @Override
    public int getInt(int key) {
        Object object = get(key);
        if (object == null) {
            return defaultInt;
        }
        return (Integer) object;
    }

    @Override
    public int getDefaultInt() {
        return defaultInt;
    }

    @Override
    public String getString(int key) {
        Object object = get(key);
        if (object == null) {
            return defaultString;
        }
        return (String) object;
    }

    @Override
    public String getDefaultString() {
        return defaultString;
    }

    @Override
    public int[] getKeys(Serializable value) {
        if (this.size == 0) {
            return null;
        } else {
            if (this.inverses == null) {
                this.calculateInverses();
            }
            return this.inverses.get(value);
        }
    }

    @Override
    public Serializable[] getValues() {
        if (this.array != null) {
            return this.array;
        } else {
            return this.map != null ? this.map.values().toArray(new Serializable[0]) : null;
        }
    }

    private void calculateInverses() {
        HashMap<Serializable, List<Integer>> inverse = new HashMap<>();
        if (this.array != null) {
            for (int index = 0; index < this.array.length; ++index) {
                if (this.array[index] != null) {
                    Serializable value = this.array[index];
                    List<Integer> list =
                        inverse.computeIfAbsent(value, llist -> new LinkedList<>());
                    list.add(index);
                } else {
                    if (this.map == null) {
                        throw new IllegalStateException();
                    }
                    Map.Entry<Integer, Serializable> mapEntry;
                    List<Integer> var6;
                    for (Iterator<Map.Entry<Integer, Serializable>> iterator =
                         this.map.entrySet().iterator(); iterator.hasNext();
                         var6.add(mapEntry.getKey())) {
                        mapEntry = iterator.next();
                        Serializable list = mapEntry.getValue();
                        var6 = inverse.computeIfAbsent(list, k -> new LinkedList<>());
                    }
                }
            }
        }
        this.inverses = new HashMap<>();
        int[] sortedKeys;
        Map.Entry<Serializable, List<Integer>> mapEntry;
        for (Iterator<Map.Entry<Serializable, List<Integer>>> iterator =
             inverse.entrySet().iterator(); iterator.hasNext();
             this.inverses.put(mapEntry.getKey(), sortedKeys)) {
            mapEntry = iterator.next();
            List var12 = mapEntry.getValue();
            sortedKeys = new int[var12.size()];
            int var7 = 0;
            Integer var9;
            for (Iterator var8 = var12.iterator(); var8.hasNext(); sortedKeys[var7++] = var9) {
                var9 = (Integer) var8.next();
            }
            if (this.array == null) {
                Arrays.sort(sortedKeys);
            }
        }
    }

    @Override
    public String toString() {
        if (size == 0) {
            return "EnumDefinition(id: " + id + ", empty)";
        }
        return "EnumDefinition(id:" + id + ", size:" + size + ", key type: " + keyType +
            ", value type: " + valueType + ')';
    }
}