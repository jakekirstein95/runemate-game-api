package com.runemate.game.api.hybrid.entities.details;

import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.util.calculations.*;
import javax.annotation.*;

/**
 * An entity that can be located on the world-graph
 */
public interface Locatable {
    /**
     * The exact coordinate position of this entity on the world-graph
     *
     * @return The coordinate position, or null if unavailable
     */
    @Nullable
    Coordinate getPosition();

    /**
     * The exact coordinate position of this entity on the region-graph (accurate to hundreds of units per tile)
     */
    @Nullable
    Coordinate.HighPrecision getHighPrecisionPosition();

    /**
     * The position of this entity relative to the region base
     */
    @Nullable
    default Coordinate.RegionOffset getLocalPosition() {
        final var position = getPosition();
        return position != null ? position.offset() : null;
    }

    /**
     * The coordinate area occupied on the world-graph
     *
     * @return The coordinate area, or null if unavailable
     */
    @Nullable
    Area.Rectangular getArea();

    /**
     * Calculates the distance to the given Locatable from this locatable using Distance.between(Locatable, Locatable).
     */
    default double distanceTo(final Locatable location) {
        return Distance.between(this, location);
    }

    /**
     * Calculates the distance to the given Locatable from this locatable using Distance.between(Locatable, Locatable, Algorithm).
     */
    default double distanceTo(final Locatable location, Distance.Algorithm algorithm) {
        return Distance.between(this, location, algorithm);
    }
}
