package com.runemate.game.api.hybrid.queries.results;

import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import java.util.*;
import java.util.concurrent.*;

public class ChatboxQueryResults extends QueryResults<Chatbox.Message, ChatboxQueryResults> {

    public ChatboxQueryResults(Collection<? extends Chatbox.Message> results) {
        super(results);
    }

    public ChatboxQueryResults(
        Collection<? extends Chatbox.Message> results,
        ConcurrentMap<String, Object> cache
    ) {
        super(results, cache);
    }

    @Override
    protected ChatboxQueryResults get() {
        return this;
    }
}
