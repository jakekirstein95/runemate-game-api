package com.runemate.game.api.hybrid.net;

import com.google.gson.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.script.data.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.io.*;
import java.io.*;
import java.net.*;
import java.util.*;
import lombok.extern.log4j.*;

@Log4j2
public class HiScores {

    public static PlayerInfo lookup(String playerName, PlayerType playerType) {
        return new PlayerInfo(playerName, playerType);
    }

    private static long parseOrDefault(String string, long def) {
        return string.matches("^\\d+$") ? Long.parseLong(string) : def;
    }

    public enum PlayerType {
        RS3(GameType.RS3, ""),
        RS3_IRONMAN(GameType.RS3, "_ironman"),
        RS3_HARDCORE_IRONMAN(GameType.RS3, "_hardcore_ironman"),
        OSRS(GameType.OSRS, "_oldschool"),
        OSRS_IRONMAN(GameType.OSRS, "_oldschool_ironman"),
        OSRS_ULTIMATE_IRONMAN(GameType.OSRS, "_oldschool_ultimate");

        private final GameType gameType;
        private final String lookupURL;

        PlayerType(GameType gameType, String urlSuffix) {
            this.gameType = gameType;
            this.lookupURL = "http://services.runescape.com/m=hiscore" + urlSuffix + "/index_lite.ws?player=";
        }
    }

    public enum Skill {
        TOTAL,
        ATTACK,
        DEFENCE,
        STRENGTH,
        CONSTITUTION,
        RANGED,
        PRAYER,
        MAGIC,
        COOKING,
        WOODCUTTING,
        FLETCHING,
        FISHING,
        FIREMAKING,
        CRAFTING,
        SMITHING,
        MINING,
        HERBLORE,
        AGILITY,
        THIEVING,
        SLAYER,
        FARMING,
        RUNECRAFTING,
        HUNTER,
        CONSTRUCTION,
        SUMMONING,
        DUNGEONEERING,
        DIVINATION,
        INVENTION;

        public final static Skill[] RS3 = values();
        public final static Skill[] OSRS = Arrays.copyOf(values(), 24);

        @Override
        public String toString() {
            return name().charAt(0) + name().substring(1).toLowerCase();
        }
    }

    public enum SkillKey {
        RANK, LEVEL, EXPERIENCE;

        @Override
        public String toString() {
            return name().charAt(0) + name().substring(1).toLowerCase();
        }
    }

    public enum Activity {
        BOUNTY_HUNTER("Bounty Hunter"),
        B_H_ROGUES("Bounty Hunter Rogues"),
        DOMINION_TOWER("Dominion Tower"),
        THE_CRUCIBLE("The Crucible"),
        CASTLE_WARS("Castle Wars"),
        B_A_ATTACKERS("Barbarian Assault Attackers"),
        B_A_DEFENDERS("Barbarian Assault Defenders"),
        B_A_COLLECTORS("Barbarian Assault Collectors"),
        B_A_HEALERS("Barbarian Assault Healers"),
        DUEL_TOURNAMENT("Duel Tournament"),
        MOBILISING_ARMIES("Mobilising Armies"),
        CONQUEST("Conquest"),
        FIST_OF_GUTHIX("Fist of Guthix"),
        GG_ATHLETICS("Gielinor Games Athletics"),
        GG_RESOURCE_RACE("Gielinor Games Resource Race"),
        WE2_ARMADYL("World Event 2 Armadyl"),
        WE2_BANDOS("World Event 2 Bandos"),
        WE2_ARMADYL_KILLS("World Event 2 Armadyl Kills"),
        WE2_BANDOS_KILLS("World Event 2 Bandos Kills"),
        HEIST_ROBBERS_CAUGHT("Heist Robbers Caught"),
        HEIST_LOOT_STOLEN("Heist Loot Stolen"),
        CFP_5_GAME_AVERAGE("Cabbage Facepunch Bonanza 5 Game Average"),
        AF15_COW_TIPPING("April Fools 2015 Cow Tipping"),
        AF15_RATS_KILLED("April Fools 2015 Rats Killed");

        private final String name;

        Activity(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public enum ActivityKey {
        RANK, SCORE
    }

    public static class PlayerInfo {

        private final String name;
        private final PlayerType playerType;
        private final Map<Skill, Map<SkillKey, Long>> skillInfo;
        private final Map<Activity, Map<ActivityKey, Long>> activityInfo;

        PlayerInfo(String playerName, PlayerType playerType) {
            this.name = playerName;
            this.playerType = playerType;
            final int skillLength = GameType.RS3 == playerType.gameType ? Skill.values().length : 24;
            this.skillInfo = new LinkedHashMap<>(skillLength);
            this.activityInfo = new LinkedHashMap<>(Activity.values().length);
            final String urlLink = playerType.lookupURL + playerName;
            try (final IOTunnel tunnel = new IOTunnel(new URL(urlLink).openStream())) {
                int lineCount = 0;
                for (String line : new String(tunnel.readAsArray()).split("\n")) {
                    if (lineCount < skillLength) {
                        Map<SkillKey, Long> value = new LinkedHashMap<>(3);
                        String[] split = line.split(",");
                        for (int i = 0; i < split.length; i++) {
                            value.put(SkillKey.values()[i], parseOrDefault(split[i], -1));
                        }
                        skillInfo.put(Skill.values()[lineCount], value);
                    } else {
                        Map<ActivityKey, Long> value = new LinkedHashMap<>(2);
                        String[] split = line.split(",");
                        for (int i = 0; i < ActivityKey.values().length; i++) {
                            value.put(ActivityKey.values()[i], parseOrDefault(split[i], -1));
                        }
                        activityInfo.put(Activity.values()[lineCount - skillLength], value);
                    }
                    lineCount++;
                }
            } catch (IOException e) {
                log.warn("Failed to lookup PlayerInfo for {}", playerName, e);
            }
        }

        public boolean exists() {
            return !skillInfo.isEmpty();
        }

        public Map<SkillKey, Long> getSkillInfo(Skill skill) {
            return skillInfo.get(skill);
        }

        public Map<ActivityKey, Long> getActivityInfo(Activity activity) {
            return activityInfo.get(activity);
        }

        public long getInfo(Skill skill, SkillKey key) {
            return skillInfo.get(skill).get(key);
        }

        public long getInfo(Activity activity, ActivityKey key) {
            return activityInfo.get(activity).get(key);
        }

        public JsonObject toJsonObject() {
            JsonObject obj = new JsonObject();
            obj.addProperty("name", name);
            JsonObject skills = new JsonObject();
            for (Map.Entry<Skill, Map<SkillKey, Long>> entry : skillInfo.entrySet()) {
                JsonObject keys = new JsonObject();
                for (Map.Entry<SkillKey, Long> keyLong : entry.getValue().entrySet()) {
                    keys.addProperty(keyLong.getKey().toString(), keyLong.getValue());
                }
                skills.add(entry.getKey().toString(), keys);
            }
            obj.add("skills", skills);
            JsonObject activites = new JsonObject();
            for (Map.Entry<Activity, Map<ActivityKey, Long>> entry : activityInfo.entrySet()) {
                JsonObject keys = new JsonObject();
                for (Map.Entry<ActivityKey, Long> keyLong : entry.getValue().entrySet()) {
                    keys.addProperty(keyLong.getKey().toString(), keyLong.getValue());
                }
                activites.add(entry.getKey().toString(), keys);
            }
            obj.add("activities", activites);
            return obj;
        }

        @Override
        public String toString() {
            return toJsonObject().toString();
        }
    }
}

