package com.runemate.game.api.osrs.location;

public enum WorldRegion {

    AMERICA("US", "USA"),
    UNITED_KINGDOM("GB", "GBR"),
    AUSTRALIA("AU", "AUS"),
    GERMANY("DE", "DEU");

    /**
     * ISO-3166-1 alpha-2 country code
     */
    private final String alpha2;
    /**
     * ISO-3166-1 alpha-3 country code
     */
    private final String alpha3;

    WorldRegion(final String alpha2, final String alpha3) {
        this.alpha2 = alpha2;
        this.alpha3 = alpha3;
    }

    public String getAlpha2() {
        return alpha2;
    }

    public String getAlpha3() {
        return alpha3;
    }

    public static WorldRegion valueOf(int location) {
        switch (location) {
            case 0:
                return AMERICA;
            case 1:
                return UNITED_KINGDOM;
            case 2:
                return AUSTRALIA;
            case 3:
                return GERMANY;
            default:
                return null;
        }
    }
}
