package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import lombok.*;

@Value
public class GroundItemSpawnedEvent implements EntityEvent {
    GroundItem groundItem;
    Coordinate coordinate;

    @Override
    public EntityType getEntityType() {
        return EntityType.GROUNDITEM;
    }
}
