package com.runemate.game.api.script.framework.listeners;

import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;

public interface ProjectileListener extends EventListener {
    default void onProjectileLaunched(ProjectileLaunchEvent event) {
    }
    default void onProjectileMoved(ProjectileMovedEvent event) {
    }
}
