package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.entities.details.*;
import lombok.*;

@Value
public class AnimationEvent implements EntityEvent {

    EntityType entityType;
    Animable source;
    int animationId;

    /**
     * @deprecated use #getEntityType()
     */
    @Deprecated
    public Type getType() {
        return Type.valueOf(entityType.name());
    }

    @Deprecated
    public enum Type {
        NPC,
        PLAYER,
        GAME_OBJECT,
        SPOT_ANIMATION,
        PROJECTILE
    }
}
