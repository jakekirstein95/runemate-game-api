package com.runemate.game.incubating.cache.animation;

import com.runemate.game.api.hybrid.cache.elements.*;

public interface Animation {
    default CompositeCacheModel apply(
        CompositeCacheModel model, int animationFrame,
        CacheAnimation stance, int stanceFrame
    ) {
        return null;
    }

    default CompositeCacheModel apply(CompositeCacheModel model, int animationFrame) {
        return null;
    }

}
