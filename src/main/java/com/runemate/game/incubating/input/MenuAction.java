package com.runemate.game.incubating.input;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.internal.*;
import java.util.regex.*;
import lombok.*;
import lombok.extern.log4j.*;

@Builder
@ToString
@InternalAPI
@Log4j2
public class MenuAction {

    @Builder.Default
    private final String option = "Automated";
    @Builder.Default
    private final String target = "";
    private final int identifier;
    private final int param0;
    private final int param1;
    private final int opcode;
    @Builder.Default
    private int clickX = -1;
    @Builder.Default
    private int clickY = -1;
    @Builder.Default
    private int itemId = -1;
    @Builder.Default
    private Interactable entity = null;
    

    private static int getItemId(int identifier, int opcode, int param0, int param1, int currentItemId)
    {
        switch (opcode)
        {
            case 1006:
                currentItemId = 0;
                break;
            case 25:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 58:
            case 1005:
                currentItemId = getItemId(param0, param1, currentItemId);
                break;

            case 57:
            case 1007:
                if (identifier >= 1 && identifier <= 10)
                {
                    currentItemId = getItemId(param0, param1, currentItemId);
                }

                break;
        }

        return currentItemId;
    }

    private static int getItemId(int param0, int param1, int currentItemId)
    {
        int container = param1 >>> 16, componentId = param1 & 0xFFFF;
        InterfaceComponent component = Interfaces.getAt(container, componentId);
        if (component != null)
        {
            var children = component.getChildren();
            if (children != null && children.size() >= 2 && container == 387)
            {
                param0 = 1;
            }

            var child = param0 == -1 ? component : component.getChild(param0);
            if (child != null)
            {
                if (currentItemId != child.getContainedItemId())
                {
                    return child.getContainedItemId();
                }
            }
        }

        return currentItemId;
    }

    public static void sendAction(String option, String target, int identifier, int opcode, int param0, int param1, int clickX, int clickY) {
        OpenClient.sendAction(param0, param1, opcode, identifier, getItemId(identifier, opcode, param0, param1, -1), option, target, clickX, clickY);
    }

    public void send() {
        if (entity != null && (clickX == -1 || clickY == -1)) {
            final var point = entity.getInteractionPoint();
            if (point != null) {
                clickX = point.x;
                clickY = point.y;
            }
        }
        sendAction(option, target, identifier, opcode, param0, param1, clickX, clickY);
    }

    public static MenuAction forInterfaceComponent(InterfaceComponent component, int action) {
        return MenuAction.builder()
            .identifier(MenuOpcode.getComponentMenuIdentifier(component, action))
            .opcode(MenuOpcode.getComponentOpcode(component))
            .param0(component.getParentComponent() == null ? -1 : component.getIndex())
            .param1(component.getId())
            .itemId(-1)
            .entity(component)
            .build();
    }

    public static MenuAction forInterfaceComponent(InterfaceComponent component, int action, MenuOpcode opcode) {
        return MenuAction.builder()
            .identifier(MenuOpcode.getComponentMenuIdentifier(component, action))
            .opcode(opcode.getId())
            .param0(component.getParentComponent() == null ? -1 : component.getIndex())
            .param1(component.getId())
            .itemId(-1)
            .entity(component)
            .build();
    }

    public static MenuAction forInterfaceComponent(InterfaceComponent component, String action) {
        return MenuAction.builder()
            .identifier(MenuOpcode.getComponentMenuIdentifier(component, action))
            .opcode(MenuOpcode.getComponentOpcode(component))
            .param0(component.getParentComponent() == null ? -1 : component.getIndex())
            .param1(component.getId())
            .itemId(-1)
            .entity(component)
            .build();
    }

    public static MenuAction forInterfaceComponent(InterfaceComponent component, Pattern action) {
        return MenuAction.builder()
            .identifier(MenuOpcode.getComponentMenuIdentifier(component, action))
            .opcode(MenuOpcode.getComponentOpcode(component))
            .param0(component.getParentComponent() == null ? -1 : component.getIndex())
            .param1(component.getId())
            .itemId(-1)
            .entity(component)
            .build();
    }

    public static MenuAction forPlayer(Player component, int action) {
        final var index = OpenPlayer.indexOf(((Entity) component).uid);
        if (index == -1) {
            return null;
        }
        return MenuAction.builder()
            .identifier(index)
            .opcode(MenuOpcode.getPlayerOpcode(action))
            .param0(0)
            .param1(0)
            .entity(component)
            .build();
    }

    public static MenuAction forPlayer(Player component, MenuOpcode opcode) {
        final var index = OpenPlayer.indexOf(((Entity) component).uid);
        if (index == -1) {
            return null;
        }
        return MenuAction.builder()
            .identifier(index)
            .opcode(opcode.getId())
            .param0(0)
            .param1(0)
            .entity(component)
            .build();
    }

    public static MenuAction forPlayer(Player component, String action) {
        final var index = OpenPlayer.indexOf(((Entity) component).uid);
        if (index == -1) {
            return null;
        }
        return MenuAction.builder()
            .identifier(index)
            .opcode(MenuOpcode.getPlayerOpcode(action))
            .param0(0)
            .param1(0)
            .entity(component)
            .build();
    }

    public static MenuAction forPlayer(Player component, Pattern action) {
        final var index = OpenPlayer.indexOf(((Entity) component).uid);
        if (index == -1) {
            return null;
        }
        return MenuAction.builder()
            .identifier(index)
            .opcode(MenuOpcode.getPlayerOpcode(action))
            .param0(0)
            .param1(0)
            .entity(component)
            .build();
    }

    public static MenuAction forNpc(Npc component, int action) {
        final var index = OpenNpc.indexOf(((Entity) component).uid);
        if (index == -1) {
            return null;
        }
        return MenuAction.builder()
            .identifier(index)
            .opcode(MenuOpcode.getNpcOpcode(action))
            .param0(0)
            .param1(0)
            .entity(component)
            .build();
    }

    public static MenuAction forNpc(Npc component, MenuOpcode opcode) {
        final var index = OpenNpc.indexOf(((Entity) component).uid);
        if (index == -1) {
            return null;
        }
        return MenuAction.builder()
            .identifier(index)
            .opcode(opcode.getId())
            .param0(0)
            .param1(0)
            .entity(component)
            .build();
    }

    public static MenuAction forNpc(Npc component, String action) {
        final var index = OpenNpc.indexOf(((Entity) component).uid);
        if (index == -1) {
            return null;
        }
        return MenuAction.builder()
            .identifier(index)
            .opcode(MenuOpcode.getNpcOpcode(component, action))
            .param0(0)
            .param1(0)
            .entity(component)
            .build();
    }

    public static MenuAction forNpc(Npc component, Pattern action) {
        final var index = OpenNpc.indexOf(((Entity) component).uid);
        if (index == -1) {
            return null;
        }
        return MenuAction.builder()
            .identifier(index)
            .opcode(MenuOpcode.getNpcOpcode(component, action))
            .param0(0)
            .param1(0)
            .entity(component)
            .build();
    }

    public static MenuAction forGameObject(GameObject component, MenuOpcode opcode) {
        final var area = component.getArea();
        if (area == null) {
            return null;
        }
        final var position = area.getBottomLeft();
        final var offset = position.offset();
        return MenuAction.builder()
            .identifier(component.getId())
            .opcode(opcode.getId())
            .param0(offset.getX())
            .param1(offset.getY())
            .entity(component)
            .build();
    }

    public static MenuAction forGameObject(GameObject component, int action) {
        return forGameObject(component, MenuOpcode.of(MenuOpcode.getGameObjectOpcode(action)));
    }

    public static MenuAction forGameObject(GameObject component, String action) {
        return forGameObject(component, MenuOpcode.getGameObjectOpcode(component, action));
    }

    public static MenuAction forGameObject(GameObject component, Pattern action) {
        return forGameObject(component, MenuOpcode.getGameObjectOpcode(component, action));
    }

    public static MenuAction forGroundItem(GroundItem component, MenuOpcode opcode) {
        final var position = component.getPosition();
        if (position == null) {
            return null;
        }
        final var offset = position.offset();
        return MenuAction.builder()
            .identifier(component.getId())
            .opcode(opcode.getId())
            .param0(offset.getX())
            .param1(offset.getY())
            .entity(component)
            .build();
    }

    public static MenuAction forGroundItem(GroundItem component, int action) {
        return forGroundItem(component, MenuOpcode.of(MenuOpcode.getGroundItemOpcode(action)));
    }

    public static MenuAction forGroundItem(GroundItem component, String action) {
        return forGroundItem(component, MenuOpcode.getGroundItemOpcode(component, action));
    }

    public static MenuAction forGroundItem(GroundItem component, Pattern action) {
        return forGroundItem(component, MenuOpcode.getGroundItemOpcode(component, action));
    }

    public static MenuAction forSpriteItem(SpriteItem item, int action) {
        final InterfaceComponent component = componentOfItem(item);
        if (component == null) {
            return null;
        }
        return MenuAction.builder()
            .identifier(action == 0 ? 0 : action + 1)
            .opcode(MenuOpcode.getItemOpcode(component, action))
            .param0(item.getIndex())
            .param1(component.getId())
            .itemId(item.getId())
            .entity(item)
            .build();
    }

    public static MenuAction forSpriteItem(SpriteItem item, String action) {
        final InterfaceComponent component = componentOfItem(item);
        if (component == null) {
            return null;
        }
        final var index = MenuOpcode.getItemActionIndex(component, action);
        return MenuAction.builder()
            .identifier(index == 0 ? 0 : index + 1)
            .opcode(MenuOpcode.getItemOpcode(component, index))
            .param0(item.getIndex())
            .param1(component.getId())
            .itemId(item.getId())
            .entity(item)
            .build();
    }

    public static MenuAction forSpriteItem(SpriteItem item, Pattern action) {
        final InterfaceComponent component = componentOfItem(item);
        if (component == null) {
            return null;
        }
        final var index = MenuOpcode.getItemActionIndex(component, action);
        return MenuAction.builder()
            .identifier(index == 0 ? 0 : index + 1)
            .opcode(MenuOpcode.getItemOpcode(component, index))
            .param0(item.getIndex())
            .param1(component.getId())
            .itemId(item.getId())
            .entity(item)
            .build();
    }

    private static InterfaceComponent componentOfItem(SpriteItem item) {
        //TODO other origin implementations
        switch (item.getOrigin()) {
            case INVENTORY:
                return Interfaces.getAt(149, 0, item.getIndex());
            default:
                return null;
        }
    }

    public static boolean setSelectedItem(SpriteItem item) {
        final var component = componentOfItem(item);
        if (component == null) {
            return false;
        }
        OpenClient.setSelectedItem(component.getId(), item.getIndex(), item.getId());
        return true;
    }

    public static boolean setSelectedSpell(Spell spell) {
        final var component = spell.getComponent();
        if (component == null) {
            return false;
        }
        OpenClient.setSelectedSpell(component.getId());
        return true;
    }

    public static void sendItemUseOn(SpriteItem item, Interactable target) {
        if (item.getOrigin() != SpriteItem.Origin.INVENTORY) {
            throw new UnsupportedOperationException("can only 'Use' items in inventory");
        }

        final var slot = componentOfItem(item);
        if (slot == null) {
            return;
        }

        MenuAction action;
        if (target instanceof InterfaceComponent) {
            action = forInterfaceComponent((InterfaceComponent) target, 0, MenuOpcode.COMPONENT_TARGET_ON_COMPONENT);
        } else if (target instanceof GameObject) {
            action = forGameObject((GameObject) target, MenuOpcode.COMPONENT_TARGET_ON_GAME_OBJECT);
        } else if (target instanceof GroundItem) {
            action = forGroundItem((GroundItem) target, MenuOpcode.COMPONENT_TARGET_ON_GROUND_ITEM);
        } else if (target instanceof Player) {
            action = forPlayer((Player) target, MenuOpcode.COMPONENT_TARGET_ON_PLAYER);
        } else if (target instanceof Npc) {
            action = forNpc((Npc) target, MenuOpcode.COMPONENT_TARGET_ON_NPC);
        } else {
            throw new IllegalArgumentException("Invalid target type " + target.getClass().getSimpleName());
        }

        if (action == null) {
            return;
        }

        OpenClient.setSelectedItem(slot.getId(), item.getIndex(), item.getId());
        action.send();
    }

    public static void sendSpellCastOn(Spell spell, Interactable target) {

        MenuAction action;
        if (target instanceof InterfaceComponent) {
            action = forInterfaceComponent((InterfaceComponent) target, 0, MenuOpcode.COMPONENT_TARGET_ON_COMPONENT);
        } else if (target instanceof GameObject) {
            action = forGameObject((GameObject) target, MenuOpcode.COMPONENT_TARGET_ON_GAME_OBJECT);
        } else if (target instanceof GroundItem) {
            action = forGroundItem((GroundItem) target, MenuOpcode.COMPONENT_TARGET_ON_GROUND_ITEM);
        } else if (target instanceof Player) {
            action = forPlayer((Player) target, MenuOpcode.COMPONENT_TARGET_ON_PLAYER);
        } else if (target instanceof Npc) {
            action = forNpc((Npc) target, MenuOpcode.COMPONENT_TARGET_ON_NPC);
        } else {
            throw new IllegalArgumentException("Invalid target type " + target.getClass().getSimpleName());
        }

        if (action == null) {
            return;
        }

        if (!setSelectedSpell(spell)) {
            return;
        }

        action.send();
    }

    public static void sendMovement(Locatable locatable) {
        if (locatable == null) {
            return;
        }

        sendMovement(locatable.getLocalPosition());
    }

    public static void sendMovement(Coordinate.RegionOffset offset) {
        if (offset == null) {
            return;
        }

        sendMovement(offset.getX(), offset.getY());
    }

    public static void sendMovement(int localX, int localY) {
        OpenClient.setDestination(localX, localY);
    }
}
