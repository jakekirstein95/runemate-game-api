package com.runemate.ui.control;

import com.google.common.base.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.util.collections.*;
import com.runemate.game.api.script.framework.listeners.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import com.runemate.ui.*;
import com.runemate.ui.control.setting.*;
import com.runemate.ui.setting.annotation.open.*;
import com.runemate.ui.setting.descriptor.open.*;
import com.runemate.ui.setting.open.*;
import com.google.common.collect.*;
import java.util.*;
import java.util.Objects;
import javafx.geometry.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import lombok.extern.log4j.*;

@Log4j2
public class SettingsGridPane extends GridPane implements SettingsListener {

    public SettingsGridPane() {
        setAlignment(Pos.TOP_LEFT);
        setGridLinesVisible(false);
        setPadding(new Insets(4, 4, 4, 24));
        setVgap(8);
        setHgap(8);

        final var textConstraints = new ColumnConstraints();
        textConstraints.setHalignment(HPos.RIGHT);
        textConstraints.setFillWidth(true);

        final var valueConstraints = new ColumnConstraints();
        valueConstraints.setHalignment(HPos.LEFT);
        valueConstraints.setFillWidth(true);

        getColumnConstraints().addAll(textConstraints, valueConstraints);
    }

    public void addItems(DefaultUI parent, SettingsManager manager, SettingsDescriptor settings, List<SettingDescriptor> items) {
        for (int row = 0; row < items.size(); row++) {
            final var item = items.get(row);
            if (item.setting().hidden()) {
                continue;
            }
            final var label = new Label(item.title());

            final Control control;
            if (item.type() == boolean.class) {
                control = new SettingCheckBox(manager, settings, item);
            } else if (item.type() == int.class) {
                control = new SettingIntSpinner(manager, settings, item);
            } else if (item.type() == double.class) {
                control = new SettingDoubleSpinner(manager, settings, item);
            } else if (item.type() == String.class) {
                if (item.setting().secret()) {
                    control = new SettingPasswordField(manager, settings, item);
                } else {
                    control = new SettingTextField(manager, settings, item);
                }
            } else if (item.type() == Coordinate.class) {
                control = new SettingPositionButton(parent.bot().getPlatform(), manager, settings, item);
            } else if (((Class<?>) item.type()).isEnum()) {
                control = new SettingComboBox(manager, settings, item);
            } else if (item.type() == EquipmentLoadout.class) {
                control = new SettingEquipmentButton(parent.bot().getPlatform(), manager, settings, item);
            } else {
                control = null;
            }

            if (control == null) {
                log.warn("Unknown setting type: {} @ {}.{}", item.type().getTypeName(), settings.group().group(), item.key());
                continue;
            }

            if (!Strings.isNullOrEmpty(item.setting().description())) {
                label.setTooltip(new Tooltip(item.setting().description()));
                control.setTooltip(new Tooltip(item.setting().description()));
            }

            label.visibleProperty().bind(control.visibleProperty());
            label.managedProperty().bind(control.managedProperty());

            add(label, 0, row);
            add(control, 1, row);
        }
    }

    @Override
    public void onSettingChanged(final SettingChangedEvent event) {
        for (var child : getChildrenUnmodifiable()) {
            if (child instanceof SettingControl) {
                ((SettingControl) child).onSettingChanged(event);
            }
        }
    }

    @Override
    public void onSettingsConfirmed() {

    }
}
