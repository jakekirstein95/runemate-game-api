package com.runemate.ui.control.setting;

import com.runemate.game.api.script.framework.listeners.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import com.runemate.ui.setting.descriptor.open.*;
import com.runemate.ui.setting.open.*;
import java.util.*;
import javafx.application.*;
import javafx.scene.*;
import javafx.scene.layout.*;

public interface SettingControl extends SettingsListener {

    SettingsManager manager();

    SettingsDescriptor group();

    SettingDescriptor setting();

    @Override
    default void onSettingsConfirmed() {

    }

    @Override
    default void onSettingChanged(final SettingChangedEvent event) {
        var dependency = setting().dependency();
        if (dependency == null) {
            return;
        }

        if (Objects.equals(dependency.group(), event.getGroup()) && Objects.equals(dependency.key(), event.getKey())) {
            Platform.runLater(() -> ((Node) this).setVisible(Objects.equals(dependency.value(), event.getValue())));
        }
    }
}
