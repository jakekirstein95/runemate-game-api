package com.runemate.ui.control.setting;

import com.runemate.ui.setting.descriptor.open.*;
import com.runemate.ui.setting.open.*;
import javafx.collections.*;
import javafx.scene.control.*;
import javafx.util.*;
import lombok.*;
import lombok.experimental.*;
import lombok.extern.log4j.*;
import org.apache.commons.text.*;

@Getter
@Log4j2
@Accessors(fluent = true)
public class SettingComboBox extends ComboBox<Enum<?>> implements SettingControl {

    private final SettingsManager manager;
    private final SettingsDescriptor group;
    private final SettingDescriptor setting;

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public SettingComboBox(final SettingsManager manager, final SettingsDescriptor group, final SettingDescriptor setting) {
        this.manager = manager;
        this.group = group;
        this.setting = setting;

        managedProperty().bind(visibleProperty());
        setDisable(setting.setting().disabled());
        setFocusTraversable(true);
        disableProperty().bind(manager.lockedProperty());

        valueProperty().addListener((obs, old, value) -> manager.set(group, setting, value.name()));

        final var type = (Class<? extends Enum>) setting.type();
        setMaxWidth(Double.MAX_VALUE);
        setItems(FXCollections.observableArrayList(type.getEnumConstants()));
        setConverter(new EnumConverter());
        final var initialText = manager.get(group, setting);

        try {
            final var initial = Enum.valueOf(type, initialText);
            if (getItems().contains(initial)) {
                getSelectionModel().select(initial);
            }
        } catch (NullPointerException e) {
            log.warn("No enum constant named '" + initialText + "' found");
        }
    }

    private static class EnumConverter extends StringConverter<Enum<?>> {

        @Override
        public String toString(final Enum<?> o) {
            String toString = o.toString();

            //base toString() impl returns name()
            if (o.name().equals(toString)) {
                return WordUtils.capitalize(toString.toLowerCase(), '_').replace("_", " ");
            }

            return toString;
        }

        @Override
        public Enum<?> fromString(final String string) {
            return null;
        }
    }
}
